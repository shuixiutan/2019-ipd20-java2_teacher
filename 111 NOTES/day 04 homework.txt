DAY 04 HOMEWORK
===============

Read about basic Git use
------------------------

https://git-scm.com/book/en/v2

Git book to read:
1.1, 1.2, 1.3
2.1, 2.2, 2.3, 2.4

>> Ignore all about command line

Things you should understand:
- commit/push/pull/clone
- working copy / staging


Sidenote: in Git your code resides in 4 places:
* Working copy
* Staging area
* Local Git Repository
* Remote Git Repository

Install SourceTree and clone your repository at home.
Remember to Commit & Push when done working at home or at school.

Liang's Java Book Chapters to Read
----------------------------------
* Objects and Classes
* Object-Oriented Thinking
* Exception Handling and Text I/O

Other Subjects to Review
------------------------
* conditional expressions
* 4 types of loop (including break and continue)
* single-dimensional arrays
* two-dimensional arrays
* method parameters and return types (signature)
* if-else, if-else-if-else chains
* switch-case
* Scanner use

Day04NamesPlus
--------------

Create new project in Netbeans called Day04NamesPlus.

Decare ArrayList<Strings> as nameList.

In a loop ask user for a name.
If name is not empty add name to the list and continue to next iteration of the loop.
If name is empty - exit the loop.

After loop is done print out all the names, semicolon-separated on a single line.


Day04Numbers
------------

Ask user how many numbers he/she wants to generate.
Generate the numbers as random integers in -100 to +100 range, both inclusive.
Place the numbers in ArrayList<Integer> named numsList.

In the next loop find the numbers that are less or equal to 0 and print them out, one per line.


Day04SimpleCalc (BONUS)
-----------------------

Ask user to choose an option from menu:
1. Add
2. Subtract
3. Multiply
4. Divide

If user enters an invalid choice, other than 1-4, display error message and exit.

Otherwise ask user for 2 floating point values,
perform the requested computation and display the result.

BONUS:

Add one more option to the menu.
0. Exit

And add a loop around the entire program so that
the menu is displayed repeatedly until user asks to exit.

Suggestion: instead of do-while loop use
an infinite loop and a break/return.
