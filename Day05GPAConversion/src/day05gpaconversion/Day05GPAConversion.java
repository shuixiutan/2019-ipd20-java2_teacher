package day05gpaconversion;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Day05GPAConversion {

    static double gpaToNum(String gpaStr) {
        switch (gpaStr) {
            case "A":
                return 4.0;
            case "A-":
                return 3.7;                
            default:
                return -1;
        }
    }

    public static void main(String[] args) {
        try ( Scanner fileInput = new Scanner(new File("file.txt"))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                double gpaVal = gpaToNum(line);
                if (gpaVal == -1) {
                    System.out.println("Error parsing value, skipping: " + line);
                    continue;
                }
                //
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

}
